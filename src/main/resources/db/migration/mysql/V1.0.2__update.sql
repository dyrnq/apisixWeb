ALTER TABLE `inst` MODIFY COLUMN `agent_url`       varchar(256) DEFAULT NULL;
ALTER TABLE `inst` MODIFY COLUMN `agent_api_key`   varchar(256) DEFAULT NULL;
ALTER TABLE `inst` MODIFY COLUMN `name`            varchar(256) DEFAULT NULL;
ALTER TABLE `inst` MODIFY COLUMN `url`             varchar(256) DEFAULT NULL;
ALTER TABLE `inst` MODIFY COLUMN `api_key`         varchar(256) DEFAULT NULL;