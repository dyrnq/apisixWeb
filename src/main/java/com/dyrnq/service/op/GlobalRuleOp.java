package com.dyrnq.service.op;

import com.dyrnq.apisix.AdminClient;
import com.dyrnq.apisix.ApisixSDKException;
import com.dyrnq.apisix.domain.GlobalRule;

import java.util.ArrayList;
import java.util.List;

public class GlobalRuleOp implements Op<GlobalRule>, Sample {

    @Override
    public void del(AdminClient client, String... id) throws ApisixSDKException {
        for (String i : id) {
            client.delGlobalRule(i);
        }
    }

    @Override
    public void drop(AdminClient client) throws ApisixSDKException {
        for (GlobalRule r : client.listGlobalRules()) {
            client.delGlobalRule(r.getId());
        }
    }

    @Override
    public GlobalRule get(AdminClient client, String id) throws ApisixSDKException {
        return client.getGlobalRule(id);
    }

    @Override
    public List<GlobalRule> list(AdminClient client) throws ApisixSDKException {
        return client.listGlobalRules();
    }

    @Override
    public String encodeId(GlobalRule obj) {
        return obj.getId();
    }

    @Override
    public GlobalRule putRaw(AdminClient client, String id, String rawData) throws ApisixSDKException {
        return client.putGlobalRuleRaw(id, rawData);
    }

    @Override
    public List<GlobalRule> list(AdminClient client, String[] id) throws ApisixSDKException {
        List<GlobalRule> list = new ArrayList<>();
        for (String i : id) {
            list.add(get(client, i));
        }
        return list;
    }

    @Override
    public GlobalRule put(AdminClient client, GlobalRule obj) throws ApisixSDKException {
        return client.putGlobalRule(obj.getId(), obj);
    }

    @Override
    public Object sample() {
        return null;
    }
}
