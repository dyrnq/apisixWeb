package com.dyrnq.apisix;

public class HttpMethod {
    public static final String REQ_POST = "POST";
    public static final String REQ_GET = "GET";
    public static final String REQ_PUT = "PUT";
    public static final String REQ_PATCH = "PATCH";
    public static final String REQ_DELETE = "DELETE";
}