package com.dyrnq.apisix.domain;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Checks {

    @SerializedName("passive")
    @Expose
    private Passive passive;
    @SerializedName("active")
    @Expose
    private Active active;

    public Passive getPassive() {
        return passive;
    }

    public void setPassive(Passive passive) {
        this.passive = passive;
    }

    public Active getActive() {
        return active;
    }

    public void setActive(Active active) {
        this.active = active;
    }

}
