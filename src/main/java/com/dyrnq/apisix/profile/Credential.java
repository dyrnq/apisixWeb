package com.dyrnq.apisix.profile;

public interface Credential {
    String getToken();
}
