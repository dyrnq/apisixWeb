package com.dyrnq.dso;

import com.dyrnq.model.Manifest;
import org.noear.wood.BaseMapper;
import org.noear.wood.annotation.Db;

@Db("db1")
public interface ManifestMapper extends BaseMapper<Manifest> {

}
