package com.dyrnq.dso;

import com.dyrnq.model.Inst;
import org.noear.wood.BaseMapper;
import org.noear.wood.annotation.Db;


@Db("db1")
public interface InstMapper extends BaseMapper<Inst> {

}
