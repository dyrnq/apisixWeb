package com.dyrnq.dso;

import com.dyrnq.model.Ca;
import org.noear.wood.BaseMapper;
import org.noear.wood.annotation.Db;

@Db("db1")
public interface CaMapper extends BaseMapper<Ca> {
}
